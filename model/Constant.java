package model; 

import javafx.util.Duration; 

public class Constant{
	public static final String TITLE="Catch the Goals";
	public static final int WIDTH=800;
	public static final int HEIGHT=500;
	public static final Duration FPS=Duration.millis(2000/60);
	public static String Username = "";
	public static int Score = 0;
}
