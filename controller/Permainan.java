package controller;

import java.io.*;
import model.DB;
import model.Permainan_model;

public class Permainan{
	private int jumlah, temp= 0;
	private String [][] data= new String[25][25];
	private String error;

	/*konstruktor*/
	public Permainan(){
		this.jumlah= 0;
	}

	public int getJumlah(){
		return this.jumlah;
	}

	public String[][] getData(){
		return this.data;
	}

	public String getError(){
		return this.error;
	}

	public void login(String username){
		temp =0;
		jumlah= 0;

		try{
			Permainan_model permainan= new Permainan_model();; //menggunakan Permainan_model
			permainan.getAll();//mamanggil semua data

			while(permainan.getResult().next()){
				String nama= permainan.getResult().getString(1);
				int skor= permainan.getResult().getInt(2);

				/*memeriksa username*/
				if(nama.equals(""+username)){
					temp= 1;
				}

				data[jumlah][0]= nama;
				data[jumlah][1]= ""+skor;

				jumlah++;
			}

			/*jika user belum pernah main sebelumnya
			*maka akan ditambahkan baru ke database
			*/
			if(temp!= 1){
				tambahPemain(username, 0);
			}

			permainan.closeResult();
			permainan.closeConnection();
		}catch(Exception e){
			error= e.toString();
		}
	}

	public void tambahPemain(String username, int skor){
		try{
			Permainan_model permainan= new Permainan_model();

			permainan.insert(username, skor);

			permainan.closeResult();
			permainan.closeConnection();
		}catch(Exception e){
			error= e.toString();
		}
	}

	public void proses(){
		jumlah= 0;

		try{
			Permainan_model permainan= new Permainan_model();
			permainan.getAll();

			while(permainan.getResult().next()){
				String nama= permainan.getResult().getString(1);
				int skor= permainan.getResult().getInt(2);

				data[jumlah][0]= nama;
				data[jumlah][1]= ""+skor;

				jumlah++;
			}

			permainan.closeResult();
			permainan.closeConnection();
		}catch(Exception e){
			error= e.toString();
		}
	}

	public void updateSkor(String username, int skor){
		try{
			Permainan_model permainan= new Permainan_model();
			permainan.updateCitacita(username, skor);

			permainan.closeResult();
			permainan.closeConnection();
		}catch(Exception e){
			error= e.toString();
		}
	}
}