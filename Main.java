import view.Gameplay;
import view.Audio;
import javafx.application.Application;
import javafx.stage.Stage;


public class Main extends Application{

	@Override
	public void start(Stage stage) throws Exception {
		Gameplay layout = new Gameplay();
		layout.start(stage);

		Audio bgm = new Audio();
		bgm.setFile("view/sound/musik.wav");
		bgm.soundPlay();
	}

	public static void main(String[] args){
		launch(args);
	}
}