package view;

import model.Constant;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.JFrame;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;

import controller.*;

public class Gameplay_main extends JFrame{
	/*menggunakan ukuran yang ada di Constant*/
	static final int WIDTH= Constant.WIDTH;
	static final int HEIGHT= Constant.HEIGHT;

	int warna= 0;

	/*menggunakan controller KeyboardInput untuk penggunaan keyboard*/
	KeyboardInput keyboard= new KeyboardInput();

	/*inisialisasi canvas*/
	Canvas canvas;

	/*objek rintangan dalam bentuk list array*/
	ArrayList<Musuh> rintangan= new ArrayList<Musuh>();
	ArrayList<Musuh> rintangan2= new ArrayList<Musuh>();

	/*inisialisasi pemain*/
	Pejuang jack= new Pejuang();

	/*inisialisasi untuk merandom posisi rintangan*/
	Random rdm= new Random();

	int keluarMusuh= 0; //keluar musuh dalam ms
	int keluarMusuh2= 0;

	/*konstruktor*/
	public Gameplay_main(){
		setIgnoreRepaint(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		canvas= new Canvas();
		canvas.setSize(WIDTH, HEIGHT);
		add(canvas);

		/*mengatur posisi windows*/
		Dimension dim= Toolkit.getDefaultToolkit().getScreenSize();//mengambil ukuran layar full screen
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);//set windows ditengah
		pack();//commit ukuran

		/*menggunakan hookup keyboard*/

		addKeyListener(keyboard);
		canvas.addKeyListener(keyboard);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
	}

	protected void inputProcess(){
		// if(keyboard.keyDown(KeyEvent.VK_DOWN)){
		// 	jack.y+= jack.dy;

		// 	if(jack.y2+ jack.h+20> HEIGHT-1)
		// 		jack.y= HEIGHT- (jack.h+48)-1;
		// }
		if(keyboard.keyDown(KeyEvent.VK_RIGHT)){
			jack.x+= jack.dx;

			if(jack.x+ jack.w> WIDTH-1)
				jack.x= WIDTH- jack.w- 1;
		}
		if(keyboard.keyDown(KeyEvent.VK_UP)){
			jack.y-= jack.dy;

			if(jack.y<0)
				jack.y= 0;
		}
		if(keyboard.keyDown(KeyEvent.VK_LEFT)){
			jack.x-= jack.dx;

			if(jack.x<0)
				jack.x= 0;
		}


		if(keluarMusuh>= 40){
			warna= rdm.nextInt(6) +(1);

			if(warna> 7)
				warna= 1;

			switch(warna){
				case 1:
					int y= rdm.nextInt(HEIGHT-25) +(25);
					rintangan.add(new Musuh(-20, y, 0, warna));
				break;
				case 2:
					rintangan.add(new Musuh(-20, 300, 0, warna));
				break;
				case 3:
					rintangan.add(new Musuh(-20, 250, 0, warna));
				break;
				case 4:
					rintangan.add(new Musuh(-20, 200, 0, warna));
				break;
				case 5:
					rintangan.add(new Musuh(-20, 150, 0, warna));
				break;	
				case 6:
					rintangan.add(new Musuh(-20, 100, 0, warna));
				break;	

			}		
			keluarMusuh= 0;
		}

		if(keluarMusuh2>= 50){
			warna= rdm.nextInt(6) +(1);

			if(warna> 7)
				warna= 1;

			switch(warna){
				case 1:
					int y= rdm.nextInt(HEIGHT-30) +(25);
					rintangan2.add(new Musuh(820, y, 0, warna));
				break;
				case 2:
					rintangan2.add(new Musuh(820, 300, 0, warna));
				break;
				case 3:
					rintangan2.add(new Musuh(820, 250, 0, warna));
				break;
				case 4:
					rintangan2.add(new Musuh(820, 200, 0, warna));
				break;
				case 5:
					rintangan2.add(new Musuh(820, 150, 0, warna));
				break;	
				case 6:
					rintangan2.add(new Musuh(820, 100, 0, warna));
				break;	

			}		
			keluarMusuh2= 0;
		}

	}

	public void permainan(){
		canvas.createBufferStrategy(2);

		BufferStrategy buffer= canvas.getBufferStrategy();
		GraphicsEnvironment ge= GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gd= ge.getDefaultScreenDevice();// menentukan device
		GraphicsConfiguration gc= gd.getDefaultConfiguration(); 
		BufferedImage bi= gc.createCompatibleImage(WIDTH, HEIGHT);
		
		Font myFont= new Font("Comics Sans MS", 1, 17);
		Graphics graphics= null;
		Graphics2D g2d= null;

		boolean gameOver= false;

		// /*inisialisasi untuk menghilangkan rintangan*/
		ArrayList<Musuh> delMusuh= new ArrayList<>();

		ImageIcon gambar= new ImageIcon("view/icon/player.png");
		Image petarung= gambar.getImage(); 		
	
		while(true){
			try{
				keyboard.poll();

				if(keyboard.keyDownOnce(KeyEvent.VK_SPACE)|| gameOver== true){
					Permainan updateSkor= new Permainan();
					updateSkor.updateSkor(Constant.Username, Constant.Score);
					break;
				}

				g2d= bi.createGraphics();
				g2d.fillRect(0, 0, WIDTH, HEIGHT);

				g2d.setFont(myFont);
				g2d.setColor(Color.BLACK);
				g2d.drawString( "Username : "+Constant.Username, 20, 20 );
          		g2d.drawString( "Energi : "+Constant.Score, WIDTH-100,  20 );

				g2d.drawString("Tekan SPACE untuk keluar", 20, 44);

				keluarMusuh++;
				keluarMusuh2++;

				jack.y= jack.y+2;

				if(jack.y2+ jack.h+25> HEIGHT-1)
					jack.y= HEIGHT- (jack.h+48)-1;


				inputProcess();

				for(Musuh p : rintangan){
					p.x= p.x+3;
    			    if(p.warna==1){
    				    g2d.setColor( Color.BLACK );
    			    }else if(p.warna==2){
    				    g2d.setColor( Color.YELLOW );
    			    }else if(p.warna==3){
    				    g2d.setColor( Color.GREEN );
    			    }else if(p.warna==4){
    				    g2d.setColor( Color.RED );
    			    }else if(p.warna==5){
    				    g2d.setColor( Color.PINK );
    			    }else if(p.warna==6){
    				    g2d.setColor( Color.BLUE );
    			    }

					g2d.fillOval(p.x, p.y, 45, 30);

					if(p.y+15>= jack.y && p.y+20<= jack.y+70 && p.x+30>= jack.x && p.x< jack.x+95){
						if(p.warna== 1){
							g2d.drawString("GAME OVER!!!", 100, 100);
							gameOver= true;
						}else if(p.warna== 2){
							p.status= 1;
							delMusuh.add(p);
							Constant.Score+= 10;
						}else if(p.warna== 3){
							p.status= 1;
							delMusuh.add(p);
							Constant.Score+= 20;
						}else if(p.warna== 4){
							p.status= 1;
							delMusuh.add(p);
							Constant.Score+= 30;
						}else if(p.warna== 5){
							p.status= 1;
							delMusuh.add(p);
							Constant.Score+= 35;
						}else if(p.warna== 6){
							p.status= 1;
							delMusuh.add(p);
							Constant.Score+= 40;
						}
					}
				}

				for(Musuh p2 : rintangan2){
					p2.x2= p2.x2-3;
    			    if(p2.warna==1){
    				    g2d.setColor( Color.BLACK );
    			    }else if(p2.warna==2){
    				    g2d.setColor( Color.YELLOW );
    			    }else if(p2.warna==3){
    				    g2d.setColor( Color.GREEN );
    			    }else if(p2.warna==4){
    				    g2d.setColor( Color.RED );
    			    }else if(p2.warna==5){
    				    g2d.setColor( Color.PINK );
    			    }else if(p2.warna==6){
    				    g2d.setColor( Color.BLUE );
    			    }

					g2d.fillOval(p2.x2, p2.y, 45, 30);


					if(p2.y+15>= jack.y && p2.y+15<= jack.y+70 && p2.x2+30>= jack.x && p2.x2< jack.x+95){
						if(p2.warna== 1){
							g2d.drawString("GAME OVER!!!", 100, 100);
							gameOver= true;
						}else if(p2.warna== 2){
							p2.status= 1;
							delMusuh.add(p2);
							Constant.Score+= 10;
						}else if(p2.warna== 3){
							p2.status= 1;
							delMusuh.add(p2);
							Constant.Score+= 20;
						}else if(p2.warna== 4){
							p2.status= 1;
							delMusuh.add(p2);
							Constant.Score+= 30;
						}else if(p2.warna== 5){
							p2.status= 1;
							delMusuh.add(p2);
							Constant.Score+= 35;
						}else if(p2.warna== 6){
							p2.status= 1;
							delMusuh.add(p2);
							Constant.Score+= 40;
						}
					}
				}

				for(Musuh p: delMusuh){
					rintangan.remove(p);
				}
				for(Musuh p2: delMusuh){
					rintangan2.remove(p2);
				}


	            jack.x2 = jack.x-5;
	            jack.y2 = jack.y+25;

	            g2d.drawImage(petarung, jack.x2, jack.y2, jack.w+30, jack.h+20, null);


				graphics= buffer.getDrawGraphics();
				graphics.drawImage(bi, 0, 0, null);
				if(!buffer.contentsLost())
					buffer.show();

				try{
					Thread.sleep(10);
				}catch(InterruptedException e){

				}
			}finally{
				if(graphics!= null)
					graphics.dispose();
				
				if(g2d!= null)
					g2d.dispose();
			}
		}
	}
}