package view;

import controller.Permainan;
import model.Constant;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.paint.Color;
import javafx.scene.text.FontWeight;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.scene.Group;
import javafx.stage.Stage;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.FadeTransition;
import javafx.util.Duration;
import javafx.event.Event;
import java.awt.geom.Ellipse2D;
import java.awt.*;
import javax.swing.*;
import javafx.beans.property.SimpleStringProperty;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.scene.image.ImageView;

import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;


import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Gameplay extends Application{
	public static class Datapemain{
		private final SimpleStringProperty Nama;	
		private final SimpleStringProperty Citacita;
		
		//konstruktor
		private Datapemain(String NM, String CC){
			this.Nama = new SimpleStringProperty(NM);
			this.Citacita = new SimpleStringProperty(CC);
		}

		//fungsi untuk mendapat nama
		public String getNama(){
			return Nama.get();
		}

		public void setNama(String NM){
			Nama.set(NM);
		}

		//fungsi untuk mendapat citacita
		public String getCitacita(){
			return Citacita.get();
		}	

		public void setCitacita(String CC){
			Citacita.set(CC);
		}

	}
	private TableView<Datapemain> table= new TableView<Datapemain>();
	private final ObservableList<Datapemain> data1 = FXCollections.observableArrayList();

	public void start(Stage stage){
		Permainan permainan= new Permainan();

		Group root= new Group();
		Scene login= new Scene(root, 550, 450, Color.WHITE);

		Label judul= new Label("CATCH THE GOALS PLEASE");
		judul.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 20));
		judul.setLayoutX(130);
		judul.setLayoutY(10);

		Label nama = new Label("Username");
		nama.setFont(Font.font("Comic Sans MS",FontWeight.BOLD, 14));
		nama.setLayoutX(10);
		nama.setLayoutY(80);

		TextField txtnama = new TextField();
		txtnama.setFont(Font.font("Comic Sans MS",FontWeight.BOLD, 14));
		txtnama.setLayoutX(90);
		txtnama.setLayoutY(75);

		Button bMain = new Button("Main");
		bMain.setFont(Font.font("Comic Sans MS",FontWeight.BOLD, 16));
		bMain.setPrefSize(100,50);
		bMain.setLayoutX(320);
		bMain.setLayoutY(65);

		Button bKeluar = new Button("Keluar");
		bKeluar.setFont(Font.font("Comic Sans MS",FontWeight.BOLD, 16));
		bKeluar.setPrefSize(100,50);
		bKeluar.setLayoutX(445);
		bKeluar.setLayoutY(65);

		TableColumn tNama = new TableColumn("Username");
		tNama.setMinWidth(150);
		tNama.setCellValueFactory(
			new PropertyValueFactory<Datapemain, String>("Nama")
		);

		TableColumn tcitacita = new TableColumn("Skor cita-cita");
		tcitacita.setMinWidth(150);
		tcitacita.setCellValueFactory(
			new PropertyValueFactory<Datapemain, String>("Citacita")
		);

		table.getColumns().setAll(tNama, tcitacita);
		table.setMaxHeight(200);
		table.setLayoutX(70);
		table.setLayoutY(150);


		int jumlah= 0, i= 0;
		String[][] data= new String[25][25];

		try{
			permainan.proses();
			jumlah= permainan.getJumlah();
			data= permainan.getData();
		}catch(Exception e){
			System.out.println(permainan.getError());
		}

		for(i=0; i<jumlah; i++){
			table.getItems().clear();
		}

		for(i=0; i<jumlah; i++){
			data1.add(new Datapemain(data[i][0],data[i][1]));
		}

		table.setItems(data1);
		root.getChildren().addAll(judul, nama, txtnama, bMain, bKeluar, table);



		bMain.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e){
				if(txtnama.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Maaf username tidak boleh kosong!!!");//untuk memberi tahu bahwa username tidak boleh kosong
				}else{
					permainan.login(txtnama.getText());

					Group root2= new Group();
					Gameplay_main app= new Gameplay_main();
				
					Constant.Username= txtnama.getText();
					Constant.Score= 0;

					app.setTitle(Constant.TITLE);
					app.setVisible(true);
					stage.close();

					root.getChildren().clear();
					app.permainan();
					app.setVisible(false);
					app.dispose();

					start(stage);
				}
			}	
		});

		bKeluar.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e){
				stage.close();
			}
		});

		stage.setScene(login);
		stage.show();
	}
}